import React from "react";
import "./App.css";
import Navbar from "./components/layout/Navbar";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <h1>Hello from React</h1>
        <Navbar />
      </div>
    );
  }
}

export default App;
